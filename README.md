## WPezTraits: Filter the Content

__You really should roll your own content filter.__ 

As inspired by: https://themehybrid.com/weblog/how-to-apply-content-filters


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --