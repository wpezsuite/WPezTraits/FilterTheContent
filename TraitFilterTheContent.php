<?php

/**
 * You really should roll your own content filter.
 *
 * ref - https://themehybrid.com/weblog/how-to-apply-content-filters
 */

namespace WPez\WPezTraits\FilterTheContent;

trait TraitFilterTheContent {

    protected function filterTheContent( $str = false ) {

        if ( $str !== false ) {

            $str = wptexturize( $str );
            $str = convert_smilies( $str );
            $str = convert_chars( $str );
            $str = wpautop( $str );
            $str = shortcode_unautop( $str );
            $str = do_shortcode( $str );

            return $str;
        }
    }
}